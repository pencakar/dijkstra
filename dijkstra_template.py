import pprint
import heapq


class Vertex:
    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.predecessor = None
        self.cost = None
        self.edges = []

class Edge:
    def __init__(self, source, target, weight):
        self.sourceId = source
        self.targetId = target
        self.weight =weight

class Dijkstra:
    def __init__(self):
        self.dict = {}
        self.root = None
        self.pq = []

    def computePath(self, sourceId):
        #initialize the sourceId to cost 0
        self.root = self.dict[sourceId]
        self.root.cost = 0

        #initialize the priority queue with all the edges of the root node
        #take the edge with the lowest cost
        edgelist = []
        for e in self.root.edges:
            heapq.heappush(self.pq, (e.weight, e))
        #pprint.pprint(self.pq)

        #go through whole heap
        while len(self.pq):
            #pop the smallest tupple from the heap
            currentTuple = heapq.heappop(self.pq)
            #print('current: ' + str(currentTuple[1].weight))

            #some variables saving the objects
            current = currentTuple[1]
            currentSourceId = current.sourceId
            currentTargetId = current.targetId
            currentWeight = current.weight
            currentSource = self.dict[currentSourceId]
            currentTarget = self.dict[currentTargetId]

            """print("Current:")
            print("  Source: " + str(currentSource))
            print("  Target: " + str(currentTarget))
            print("  Weight: " + str(currentWeight))
            print("\n")"""

            #check if the target vertex was yet discovered
            if not currentTarget.cost == None:
                continue
            else:

                print("Not discovered: " + str(currentWeight))
                #set the target cost to source cost + edge weight cost
                currentTarget.cost = currentSource.cost + currentWeight

                #for all edges of the current vertex
                for e in currentTarget.edges:
                    #add the edge to the heap
                    heapq.heappush(self.pq, (e.weight, e))
                    print(e.weight)

            print("\n")






    def getShortestPathTo(self, targetId):
        pass

    def createGraph(self, vertexes, edgesToVertexes):
        #edges to vertexes
        for v in vertexes:
            for e in edgesToVertexes:
                if v.id == e.sourceId:
                    v.edges.append(e)

        #map from vertex id to vertex object (dict)

        for v in vertexes:
            self.dict[v.id] = v

    def resetDijkstra(self):
        for vertexId in self.dict:
            vertexObj = self.dict[vertexId]



    def getVertexes(self):
        pass




"""Test:"""
def runTests():
    redville = Vertex(0, "Redville")
    blueville = Vertex(1, "Blueville")
    greenville = Vertex(2, "Greenville")
    orangeville = Vertex(3, "Orangeville")
    purpleville = Vertex(4, "Purpleville")
    vertexes = [redville, blueville, greenville, orangeville, purpleville]

    edgesRed = [Edge(0, 1, 5), Edge(0, 2, 10), Edge(0, 3, 8)]
    edgesBlue = [Edge(1, 0, 5), Edge(1, 2, 3), Edge(1, 4, 7)]
    edgesGreen = [Edge(2, 0, 10), Edge(2, 1, 3)]
    edgesOrange = [Edge(3, 0, 8), Edge(3, 4, 2)]
    edgesPurple = [Edge(4, 3, 2), Edge(4, 1, 7)]
    edges = edgesRed + edgesBlue + edgesGreen + edgesOrange + edgesPurple

    dijkstra = Dijkstra()
    dijkstra.createGraph(vertexes, edges)
    dijkstra.computePath(0)

runTests()